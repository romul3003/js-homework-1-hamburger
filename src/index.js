Hamburger.SIZE_SMALL = 'SIZE_SMALL'
Hamburger.SIZE_LARGE = 'SIZE_LARGE'
Hamburger.STUFFING_CHEESE = 'STUFFING_CHEESE'
Hamburger.STUFFING_SALAD = 'STUFFING_SALAD'
Hamburger.STUFFING_POTATO = 'STUFFING_POTATO'
Hamburger.TOPPING_MAYO = 'TOPPING_MAYO'
Hamburger.TOPPING_SPICE = 'TOPPING_SPICE'

Hamburger.data = {
	hamburgerSize: {
		'SIZE_SMALL': {
			price: 50,
			calories: 20
		},
		'SIZE_LARGE': {
			price: 100,
			calories: 40
		}
	},
	hamburgerStuffing: {
		'STUFFING_CHEESE': {
			price: 10,
			calories: 20
		},
		'STUFFING_SALAD': {
			price: 20,
			calories: 5
		},
		'STUFFING_POTATO': {
			price: 15,
			calories: 10
		}
	},
	hamburgerTopping: {
		'TOPPING_SPICE': {
			price: 15,
			calories: 0
		},
		'TOPPING_MAYO': {
			price: 20,
			calories: 5
		}
	}
}

function Hamburger(size, stuffing) {
	var reg = [/^SIZE/, /^STUFFING/]
	var exception = false

	if (arguments.length !== 2) {
		exception = true
		throw new HamburgerException('no size given')
	}

	for (var i = 0; i < arguments.length; i++) {
		if (!reg[i].test(arguments[i])) {
			exception = true
			throw new HamburgerException('invalid size ' + "'" + arguments[i] + "'")
		}
	}

	if (!exception) {
		this._size = size
		this._stuffing = stuffing
		this._toppings = []
	}
}

function HamburgerException(message) {
	this.name = 'HamburgerException: '
	this.message = message
}

HamburgerException.prototype = Object.create(Error.prototype)
HamburgerException.prototype.constructor = HamburgerException

Hamburger.prototype.addTopping = function (topping) {
	this._toppings.forEach(function (item) {
		if (item === topping) {
			throw new HamburgerException('duplicate topping ' + "'" + topping + "'")
		}
	})

	this._toppings.push(topping)
}

Hamburger.prototype.removeTopping = function (topping) {
	this._toppings.forEach(function (item, i, toppings) {
		if (item === topping) {
			toppings.splice(i, 1);
		}
	})
}

Hamburger.prototype.getToppings = function () {
	return this._toppings
}

Hamburger.prototype.getSize = function () {
	return this._size
}

Hamburger.prototype.getStuffing = function () {
	return this._stuffing
}

Hamburger.prototype.calculatePrice = function () {
	var data = this.constructor.data

	var toppingsPrice = this._toppings.reduce(function (sum, current) {
		return sum + data.hamburgerTopping[current].price
	}, 0)

	return data.hamburgerSize[this._size].price +
		data.hamburgerStuffing[this._stuffing].price + toppingsPrice
}

Hamburger.prototype.calculateCalories = function () {
	var data = this.constructor.data

	var toppingsCalories = this._toppings.reduce(function (sum, current) {
		return sum + data.hamburgerTopping[current].calories
	}, 0)

	return data.hamburgerSize[this._size].calories +
		data.hamburgerStuffing[this._stuffing].calories + toppingsCalories
}

/////////////  client interface  /////////////

try {
	// маленький гамбургер с начинкой из сыра
	var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE)

	// забыл, а какая там начинка?
	console.log("Stuffing is: %s", hamburger.getStuffing())

	// добавка из майонеза
	hamburger.addTopping(Hamburger.TOPPING_MAYO)

	// спросим сколько там калорий
	console.log("Calories: %f", hamburger.calculateCalories())

	// сколько стоит
	console.log("Price: %f", hamburger.calculatePrice())

	// я тут передумал и решил добавить еще приправу
	hamburger.addTopping(Hamburger.TOPPING_SPICE)

	// А сколько теперь стоит?
	console.log("Price with sauce: %f", hamburger.calculatePrice())

	// Проверить, большой ли гамбургер?
	console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE)

	// Убрать добавку
	hamburger.removeTopping(Hamburger.TOPPING_SPICE)
	console.log("Have %d toppings", hamburger.getToppings().length)

} catch (e) {
	console.log(e.name + e.message)
}

console.log(hamburger)
